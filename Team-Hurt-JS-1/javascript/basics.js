var $ = function(id) {return document.getElementById(id); }//end $

window.onload = function () {

	//Provide answer for Question 1 here
	document.getElementById('title').innerHTML = "JavaScript Control Structures Test Document";
	
	testQuestion2();
	testQuestion3();
	
}


function testQuestion2() {
	
	// Does the math to determine if the number is odd or even, returning a 0 or a 1.
	function deterOdd(theNum){
		return (theNum % 2);
	}

	// prompts the user at the beginning of the document for number.	
	function initPrompt(){
		var input = prompt("Enter number:");
		if (isNaN(input)){
			alert("This is not a number.");
			initPrompt();
		} else {
			endNum = parseInt(input);
		}
	}

	// Creates list for array
	function makeUL(array){
		var list = document.createElement('ul');
		for (var i = 0; i < array.length; i++){
			var item = document.createElement('li');
			item.appendChild(document.createTextNode(array[i]));
			list.appendChild(item);
		}
		return list;
	}

	// understands the input from deterOdd, giving 1 an odd and all else as even
	function typeNum(i) {
		if (deterOdd(i) == 1){
			intType = "is odd.";
		} else {
			intType = "is even.";
		}
			
	}

	// Run init.
	initPrompt();
	// Creates list for numbers.
	var listNum = [];
	for (var i = 1; i <= endNum; i++){
		theNum = i;
		deterOdd(i);
		typeNum(i);
		listNum.push(theNum + " " + intType);
	}
	// Publish makeUL to site.
	document.getElementById('title').appendChild(makeUL(listNum));
}//end testQuestion2


function testQuestion3() {

}//end testQuestion3